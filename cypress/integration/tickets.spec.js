
describe("Tickets", () =>{
    
    beforeEach(()=> cy.visit("https://bit.ly/2XSuwCW"))

    it("fills all the text input fields", ()=>{
        const firstName = "Manuel";
        const lastName = "Neto";

        cy.get("#first-name").type(firstName);
        cy.get("#last-name").type(lastName);
        cy.get("#email").type("mbn.man@gmail.com");
        cy.get("#requests").type("Vegetarian");
        cy.get("#signature").type(`${firstName} ${lastName}`);
    });

    it("Select two tickets", () => {
        cy.get("#ticket-quantity").select("2");  
    });

    it("Select 'vip' ticket type", () => {
        cy.get("#vip").check();  
    });

    it("selects 'social media' checbox", () => {
        cy.get("#social-media").check();  
    });

    it("selects 'friend', and 'publication', then uncheck 'friend'", () => {
        cy.get("#friend").check();
        cy.get("#publication").check();
        cy.get("#friend").uncheck();
    });
    
    it("has 'TICKET BOX' header's heading", () =>{
        cy.get("header h1").should("contain","TICKETBOX");
    });

    it("alerts on invlid email", () =>{
        cy.get("#email")
            .as("email")
            .type('mbn-com');

        cy.get("#email.invalid").should("exist")

        cy.get("@email")
            .clear()
            .type("mbn.man@gmail.com");        

        cy.get("#email.invalid").should("not.exist")

    });

    it("fills and reset the form", ()=>{
        const firstName = "Manuel";
        const lastName = "Neto";
        const fullName = `${firstName} ${lastName}`

        cy.get("#first-name").type(firstName);
        cy.get("#last-name").type(lastName);
        cy.get("#email").type("mbn.man@gmail.com");
        cy.get("#ticket-quantity").select("2");  
        cy.get("#vip").check();  
        cy.get("#friend").check();
        cy.get("#requests").type("IPA Beer");

        cy.get(".agreement p").should(
            "contain",
            `I, ${fullName}, wish to buy 2 VIP tickets.`
        );

        cy.get("#agree").check();

        cy.get("#signature").type(fullName);

        cy.get("button[type='submit']")
            .as("submitButton")
            .should("not.be.disabled")

        cy.get("button[type='reset']").click();

        cy.get("@submitButton").should("be.disabled");
    });

    it("fills mandatory fields using support command", () => {
        const customer = {
            firstName: "João",
            lastName: "Silva",
            email: "joaosilva@gmail.com"
        };

        cy.fillMandatoryFields(customer);

        cy.get("button[type='submit']")
            .as("submitButton")
            .should("not.be.disabled")

        cy.get("#agree").uncheck();

        cy.get("@submitButton").should("be.disabled");
    });
});